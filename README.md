# CircuitConversions

Collection of functions to convert from one network representation into another.

```matlab
Y = Convert.StoY(S,Zref)
```

where S is of size $`N_{port} \times N_{port} \times N_{freq}`$ and Zref is scalar or of size $`1 \times N_{port}`$. The result Y has the same size as S.

## Requirements

We make use of the `pagemtimes` and `pagemrdivide` functions from matlab, so minimum 2022a is required.

Most of the code is automatically generated and verified using the symbolic toolbox from Matlab, so if you want to run the generation scripts, you require that toolbox.

**WARNING:** There is a bug in Matlab `pagemrdivide` starting from version 2022b which breaks the code.
The Mathworks should be fixing this in an update.

## Definitions

Y-parameters: 
 
```math
\begin{bmatrix}
I_1 \\
I_2 \\
\end{bmatrix} = 
\mathbf{Y}
\begin{bmatrix}
V_1 \\
V_2 \\
\end{bmatrix}
```

Z-parameters: 
 
```math
\begin{bmatrix}
V_1 \\
V_2 \\
\end{bmatrix} = 
\mathbf{Z}
\begin{bmatrix}
I_1 \\
I_2 \\
\end{bmatrix}
```

S-parameters: 
 
```math
\begin{bmatrix}
B_2 \\
B_2 \\
\end{bmatrix} = 
\mathbf{S}
\begin{bmatrix}
A_1 \\
A_2 \\
\end{bmatrix}
```

H-parameters: 
 
```math
\begin{bmatrix}
V_1 \\
I_2 \\
\end{bmatrix} = 
\mathbf{H}
\begin{bmatrix}
I_1 \\
V_2 \\
\end{bmatrix}
```

G-parameters: 
 
```math
\begin{bmatrix}
I_1 \\
V_2 \\
\end{bmatrix} = 
\mathbf{G}
\begin{bmatrix}
V_1 \\
I_2 \\
\end{bmatrix}
```

A-parameters: 
 
```math
\begin{bmatrix}
V_1 \\
I_1 \\
\end{bmatrix} = 
\mathbf{A}
\begin{bmatrix}
V_2 \\
-I_2 \\
\end{bmatrix}
```

B-parameters: 
 
```math
\begin{bmatrix}
V_2 \\
-I_2 \\
\end{bmatrix} = 
\mathbf{B}
\begin{bmatrix}
V_1 \\
I_1 \\
\end{bmatrix}
```

T-parameters: 
 
```math
\begin{bmatrix}
A_1 \\
B_1 \\
\end{bmatrix} = 
\mathbf{T}
\begin{bmatrix}
B_2 \\
A_2 \\
\end{bmatrix}
```

and we assume the following relationship between the voltages and currents and the incident and reflected waves:

```math
\begin{align*}
A_1 &= \frac{V_{1} +I_{1}Z_{1}}{2\sqrt{\Re\left(Z_{1}\right)}}\qquad
B_1 &= \frac{V_{1}-I_{1}Z_{1}}{2\sqrt{\Re\left(Z_{1}\right)}}\\
A_2 &= \frac{V_{2}+I_{2}Z_{2}}{2\sqrt{\Re\left(Z_{2}\right)}}\qquad
B_2 &= \frac{V_{2}-I_{2}Z_{2}}{2\sqrt{\Re\left(Z_{2}\right)}}\\
\end{align*}
```


## Transforming between the representations

All these different parameters describe the behaviour of the circuit.
Usually, the designer chooses the representation that best suits the
task at hand. It is therefore common to have to transform the circuit
representations from one into the other. There are some references
available that provide formulas to carry out these transformations,
but copying them is an error-prone task and many of the lists of conversion
formulas contain errors [Frickey1994]. 

In general, the circuit parameters are in a certain representation
$`\mathbf{R}`$ which links the input signals $`\mathbf{U}`$ to output
signals $`\mathbf{O}`$:

```math
\mathbf{O}=\mathbf{R}\mathbf{U}
```

They have to be transformed into another representation $`\mathbf{R}^{\prime}`$
with its inputs $`\mathbf{U}^{\prime}`$ and outputs $`\mathbf{O}^{\prime}`$

```math
\mathbf{O}^{\prime} =\mathbf{R}^{\prime}\mathbf{U}^{\prime}
```

the transformation from one representation to another can be described
by looking at the transformation on the stacked input-output vectors:

```math
\left[\begin{matrix}
\mathbf{O}^{\prime} \cr
\mathbf{U}^{\prime}
\end{matrix}\right] =
\left[\begin{matrix}
\mathbf{P} _ {11} & \mathbf{P} _ {12} \cr
\mathbf{P} _ {21} & \mathbf{P} _ {22}
\end{matrix}\right]
\left[\begin{matrix}
\mathbf{O}\cr
\mathbf{U}
\end{matrix}\right]
```

The goal of the transformation is to write $`\mathbf{R}^{\prime}`$ as a function
of the original representation $`\mathbf{R}`$ and the transformation
matrix $`\mathbf{P}`$. Solving (\ref{eq:Generaltransfo}) for $`\mathbf{O}^{\prime}`$
gives

```math
\mathbf{O}^{\prime}=\left(\mathbf{P}_{11}\mathbf{R}+\mathbf{P}_{12}\right)\left(\mathbf{P}_{21}\mathbf{R}+\mathbf{P}_{22}\right)^{-1}\mathbf{U}^{\prime}
```

This give us the expression for $`\mathbf{R}^{\prime}`$ in function of $`\mathbf{R}`$:

```math
\mathbf{R}^{\prime}=\left(\mathbf{P}_{11}\mathbf{R}+\mathbf{P}_{12}\right)\left(\mathbf{P}_{21}\mathbf{R}+\mathbf{P}_{22}\right)^{-1}
```

This expression allows us to transform any representation into another
when we know the transformation matrix $`\mathbf{P}`$. The different
transformation matrices for most of the representations are listed
at the end of the paper. This approach is based on the excellent paper
on mixed-mode S-parameters where a similar transform is constructed
to transform from single-ended to mixed-mode [Ferrero2006].

### Transforming into and from wave-based representations

The transformation matrix $`\mathbf{P}`$ is not just a simple permutation
matrix when transforming from representations that use voltages and
currents to wave-based circuit representations and vice-versa. In
such transformations, voltages and currents are mixed to create the
waves, or incident and reflected waves are combined to obtain voltages
and currents. The formula to get the incident and reflected waves
from voltages and currents has been given before.
Solving this equation for $`V_{i}`$ and $`I_{i}`$ gives the relations
between the voltage and current:

```math
\begin{cases}
\vphantom{\frac{1}{2k}}A_{i} & =k\left(V_{i}+Z_0 I_{i}\right)\\
\vphantom{\frac{1}{2k}}B_{i} & =k\left(V_{i}-Z_0 I_{i}\right)
\end{cases}\Leftrightarrow\begin{cases}
V_{i}= & \frac{1}{2k}\left(A_{i}+B_{i}\right)\\
I_{i}= & \frac{1}{2k}\left(Y_0 A_{i}-Y_0 B_{i}\right)
\end{cases}
```

where $`Y_{0}=\frac{1}{Z_{0}}`$. These formulas can now be used
to obtain the transformation matrix.

## Computing the derivative

Suppose we know the deviation of the representation with respect to a real parameter $`p`$:

```math
d\mathbf{R} = \left[\begin{matrix}
\frac{\partial R _ {11}}{\partial p} & \cdots & \frac{\partial R _ {1N}}{\partial p} \\
\vdots & \ddots & \vdots \\
\frac{\partial R _ {N1}}{\partial p} & \cdots & \frac{\partial R _ {NN}}{\partial p} \\
\end{matrix}\right]
```

then it would be usefull to know the derivative of the transformed representation $`d\mathbf{R}^\prime`$. We compute the derivative as follows:

```math
d\mathbf{R}^\prime = \mathbf{P} _ {11} d\mathbf{R} (\mathbf{P} _ {21}\mathbf{R} + \mathbf{P} _ {22})^{-1} + (\mathbf{P} _ {11}\mathbf{R} + \mathbf{P} _ {12} ) d\left( (\mathbf{P} _ {21}\mathbf{R} + \mathbf{P} _ {22})^{-1} \right)
```

when using the expression for the derivative of the matrix inverse: 

```math
dX^{-1} = -X^{-1}dX X^{-1}
```

we obtain

```math
\begin{align*}
d\mathbf{R}^\prime = & \mathbf{P} _ {11} d\mathbf{R} (\mathbf{P} _ {21}\mathbf{R} + \mathbf{P} _ {22})^{-1} \\
 & - (\mathbf{P} _ {11}\mathbf{R} + \mathbf{P} _ {12} )  (\mathbf{P} _ {21}\mathbf{R} + \mathbf{P} _ {22})^{-1} \mathbf{P} _ {21} d\mathbf{R}(\mathbf{P} _ {21} \mathbf{R} + \mathbf{P} _ {22})^{-1} \\
= & (\mathbf{P} _ {11} - \mathbf{R}^\prime \mathbf{P} _ {21}) d\mathbf{R} (\mathbf{P} _ {21}\mathbf{R} + \mathbf{P} _ {22})^{-1}
\end{align*}
```


## Conclusion

A very simple-to implement method is obtained to go from one circuit
representation to another. The method uses a transformation matrix
$\mathbf{P}$, which can easily be constructed and which are listed . 
The actual transform boils down to splitting $`\mathbf{P}`$ in four parts

```math
\mathbf{P}=\left[\begin{array}{cc}
\mathbf{P}_{11} & \mathbf{P}_{12}\\
\mathbf{P}_{21} & \mathbf{P}_{22}
\end{array}\right]
```

and then calculating

```math
\mathbf{R}^{\prime}=\left(\mathbf{P}_{11}\mathbf{R}+\mathbf{P}_{12}\right)\left(\mathbf{P}_{21}\mathbf{R}+\mathbf{P}_{22}\right)^{-1}
```

where $`\mathbf{R}`$ is the matrix describing the circuit in its original
representation and $`\mathbf{R}^{\prime}`$ is the circuit matrix in the new
representation. 

# List of Transformation matrices

Now that we have details the theory, we can apply this framework to obtain the transformation formulae for each of the different representations. 
Using Matlab's symbolic toolbox, we computed the transformation formula's for all the different 2-port networks. The results can be found [here](../ConversionFormulas).

Because Y, Z and S-parameters can be done with more than two ports, we compute the matrix expression here in more detail and come up with simplifications which should significantly speed up the conversion.


we define the following helper matrices

```math
K = \left[\begin{matrix} k_1 \\ & \ddots \\ & & k_N \end{matrix}\right]
\qquad
L = \frac{1}{2}K^{-1} = \left[\begin{matrix} \frac{1}{2k_1} \\ & \ddots \\ & & \frac{1}{2k_N} \end{matrix}\right]
```

```math
Z_0 = \left[\begin{matrix} Z_{0,1} \\ & \ddots \\ & & Z_{0,N} \end{matrix}\right]
\qquad
Y_0 = Z_0^{-1}= \left[\begin{matrix} \frac{1}{Z_{0,1}} \\ & \ddots \\ & & \frac{1}{Z_{0,N}} \end{matrix}\right]
```

with these, we can write down the following formula's for the transforms:

## From Y-parameters to S and Z

We start with a trivial transform, but is shows nicely how things work

```math
\mathbf{Y} \rightarrow \mathbf{Z} : 
\left[\begin{matrix}
\mathbf{V} \\ 
\mathbf{I}
\end{matrix}\right] = 
\left[\begin{matrix}
0 _ N & I _ N \\ 
I _ N & 0 _ N
\end{matrix}\right] \left[\begin{matrix}
\mathbf{I} \\ 
\mathbf{V}
\end{matrix}\right]
```

where $0_N$ and $I_N$ are the zero-matrix and the idenity matrix respectively. Both are of size $`N\times N`$.
Plugging the 4 matrices into the general expression for the transform, we can simplify it to obtain that the Z-matrix is just the inverse of the Y-matrix:

```math
\mathbf{Z} = (0_N\mathbf{Y} + I_N)(I_N\mathbf{Y} + 0_N)^{-1} = \mathbf{Y}^{-1}
```

We can check the expression for the derivative here too by plugging our transform matrices into the expression:

```math
d\mathbf{Z} = (0_N - \mathbf{Z} I_N) d\mathbf{Y} (I_N\mathbf{Y} + 0_N)^{-1} = -\mathbf{Z} d\mathbf{Y} \mathbf{Z}
```

Going from Y to S is more interesting:

```math
\mathbf{Y} \rightarrow \mathbf{S} : 
\left[\begin{matrix}
\mathbf{B} \\ 
\mathbf{A}
\end{matrix}\right] = 
\left[\begin{matrix}
-KZ _ 0 & K \\ 
KZ _ 0 & K
\end{matrix}\right] \left[\begin{matrix}
\mathbf{I} \\ 
\mathbf{V}
\end{matrix}\right]
```

We can plug the matrices into the expression for the conversion and simplify the obtained expression somewhat to obtain an expression wich requires less calculations:

```math
\begin{align*}
\mathbf{S} & = (-KZ_0\mathbf{Y} + K)(KZ_0\mathbf{Y} + K)^{-1} \\ 
& = K (I_N -Z_0\mathbf{Y})(I_N + Z_0\mathbf{Y} )^{-1}K^{-1} \\ 
& = K Z_0(Y_0 - \mathbf{Y})(Y_0 + \mathbf{Y} )^{-1} (K Z_0)^{-1} 
\end{align*}
```

To implement this final expression efficiently, recall that left-multiplication with a diagonal matrix is the same as scaling the rows of a matrix with the diagonal elements. 

```math
\left[\begin{matrix}
A_{11} \\ 
& \ddots \\ 
& &  A_{NN}
\end{matrix}\right] 
\left[\begin{matrix}
\mathbf{B} _ {1,:} \\ 
\vdots \\ 
\mathbf{B} _ {N,:}
\end{matrix}\right] = 
\left[\begin{matrix}
A _ {11}\mathbf{B} _ {1,:} \\ 
\vdots \\ 
A _ {NN}\mathbf{B} _ {N,:}
\end{matrix}\right]
```

The same goes for right-multiplication and scaling the columns of the matrix.

```math
\left[\begin{matrix}
\mathbf{C} _ {:,1} &  \cdots & \mathbf{C} _ {:,N}
\end{matrix}\right] 
\left[\begin{matrix}
A _ {11} \\
& \ddots \\
& &  A _ {NN}
\end{matrix}\right] = 
\left[\begin{matrix}
A _ {11}\mathbf{C} _ {:,1} &  \cdots & A _ {NN}\mathbf{C} _ {:,N}
\end{matrix}\right] 
```

For the derivative, we obtain:

```math
\begin{align*}
d\mathbf{S} & = (-KZ_0 - \mathbf{S} KZ_0) d\mathbf{Y} (KZ_0\mathbf{Y} + K)^{-1} \\
& = -(I_N + \mathbf{S} )KZ_0 d\mathbf{Y} (\mathbf{Y} + Y_0)^{-1} (KZ_0)^{-1}
\end{align*}
```

## From Z-parameters to S and Y

```math
\mathbf{Z} \rightarrow \mathbf{Y} : 
\left[\begin{matrix}
I \\ 
V
\end{matrix}\right] = 
\left[\begin{matrix}
0_N & I_N \\ 
I_N & 0_N
\end{matrix}\right] \left[\begin{matrix}
\mathbf{V} \\ 
\mathbf{I}
\end{matrix}\right]
```

Pluggin this into the expression, we again confirm that the Y-matrix is the inverse of the Z-matrix.

```math
\mathbf{Z} \rightarrow \mathbf{S} : 
\left[\begin{matrix}
\mathbf{B} \\ \mathbf{A}
\end{matrix}\right] = 
\left[\begin{matrix}
K & -KZ_0 \\ K & KZ_0
\end{matrix}\right] \left[\begin{matrix}
\mathbf{V} \\ \mathbf{I}
\end{matrix}\right]
```

We simplify the expression as follows:

```math
\begin{align*}
\mathbf{S} & = ( K\mathbf{Z} -KZ_0)(K\mathbf{Z} + KZ_0)^{-1} \\ 
& = K ( \mathbf{Z} - Z_0)(\mathbf{Z} + Z_0)^{-1} K^{-1} 
\end{align*}
```

## From S-parameters to Y and Z

When transforming from S to, we use the helper matrices $`L`$ and $`Y_0`$:

```math
\mathbf{S} \rightarrow \mathbf{Y} : 
\left[\begin{matrix}
\mathbf{I} \\ \mathbf{V}
\end{matrix}\right] = 
\left[\begin{matrix}
-LY_0 & LY_0 \\ L & L
\end{matrix}\right] \left[\begin{matrix}
\mathbf{B} \\ \mathbf{A}
\end{matrix}\right] 
```

```math
\begin{align*}
\mathbf{Y} & = \left(-LY_0\mathbf{S}+LY_0\right)\left(L\mathbf{S}+L\right)^{-1}\\
           & = -LY_0\left(I_N-S\right)\left(I_N+S\right)^-1L^{-1}
\end{align*}
```

For Z-parameters, we obtain:

```math
\mathbf{S} \rightarrow \mathbf{Z}
\left[\begin{matrix}
\mathbf{V} \\ \mathbf{I}
\end{matrix}\right] = 
\left[\begin{matrix}
 L & L \\
-LY_0 & LY_0 
\end{matrix}\right] \left[\begin{matrix}
\mathbf{B} \\ \mathbf{A}
\end{matrix}\right]
```

```math
\begin{align*}
\mathbf{Z} & =\left(L\mathbf{S}+L\right)\left(-LY_0\mathbf{S}+LY_0\right)^{-1}\\
           & = L \left(\mathbf{S}+I_N\right)\left(I_N - \mathbf{S}\right)^{-1} Y_0^{-1} L^{-1}
\end{align*}
```

which is clearly the inverse of the result we obtain for Y to S.

For S-parameters, we have the special case where we can change the reference impedance of the ports. We have two reference impedances $`Z_{0,i}`$ and $`Z_{0,o}`$ with respective $`k_i`$ and $`k_o`$. We can write the following equations for the waves:

```math
\begin{align*}
A_i & = k_i (V + Z_{0,i}I)  \qquad  B_i & = k_i (V - Z_{o,i}I) \\
A_o & = k_o (V + Z_{0,o}I)  \qquad  B_o & = k_o (V - Z_{0,o}I)
\end{align*}
```

Writing the expression for $`A_o`$ and $`B_o`$ as a function of the $`A_i`$ and $`B_i`$, we obtain:

```math
\begin{align*}
A_o & = \frac{k_o}{2k_i}\left(1+\frac{Z_{0,o}}{Z_{0,i}}\right) A_i + \frac{k_o}{2k_i}\left(1-\frac{Z_{0,o}}{Z_{0,i}}\right) B_i \\
B_o & = \frac{k_o}{2k_i}\left(1-\frac{Z_{0,o}}{Z_{0,i}}\right) A_i + \frac{k_o}{2k_i}\left(1+\frac{Z_{0,o}}{Z_{0,i}}\right) B_i \\
\end{align*}
```

We therefore obtain the following conversion matrix:

```math
\mathbf{S}_i \rightarrow \mathbf{S}_o : 
\left[\begin{matrix}
\mathbf{B}_o \\ \mathbf{A}_o
\end{matrix}\right] = 
\left[\begin{matrix}
\frac{1}{2}K_oK_i^{-1}(I_N+Z_oZ_i^{-1}) & \frac{1}{2}K_oK_i^{-1}(I_N-Z_oZ_i^{-1}) \\
\frac{1}{2}K_oK_i^{-1}(I_N-Z_oZ_i^{-1}) & \frac{1}{2}K_oK_i^{-1}(I_N+Z_oZ_i^{-1}) \\
\end{matrix}\right] \left[\begin{matrix}
\mathbf{B}_i \\ \mathbf{A}_i
\end{matrix}\right]
```

plugging this into the expression and simplifying, we obtain:

```math
\mathbf{S}_o =  K_oK_i^{-1}\left[(I_N+Z_oZ_i^{-1})\mathbf{S}_i+(I_N-Z_oZ_i^{-1})\right]\left[(I_N-Z_oZ_i^{-1})\mathbf{S}_i+(I_N+Z_oZ_i^{-1})\right]^{-1} K_iK_o^{-1}
```

## Bibliography

K. Kurokawa, "Power waves and the scattering matrix," 
*IEEE Transactions   on Microwave Theory and Techniques*, 
vol. 13, no. 2, pp. 194--202, Mar 1965.

R.B. Marks and D.F. Williams, "A general waveguide circuit theory,"
*Journal of Research-National Institute of Standards and Technology*,
 vol.97, pp. 533--533, 1992.

D.A. Frickey, "Conversions between s, z, y, h, abcd, and t parameters which are valid for complex source and load impedances," 
*IEEE Transactions on Microwave Theory and Techniques*, 
  vol. 42, no. 2, pp. 205--211, Feb 1994.

A. Ferrero and M. Pirola, "Generalized mixed-mode s-parameters," 
*IEEE Tran. on Microwave Theory and Techniques*, 
vol. 54, no. 1, pp. 458--463, Jan 2006.
