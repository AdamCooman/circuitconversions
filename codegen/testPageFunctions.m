% Test to check the pagemrdivide and pagemldivide functions
% We observed differences between matlab 2022a and 2022b
% so we run the functions on random data and compute the difference to
% the normal functions in a for-loop
clear variables
close all
clc
% fix the seed to get the same result each time
rng(331989);

% size of the matrices
N = 2;
% number of matrices used
F = 3000;
% generate some random complex data
D1 = randn(N,N,F) + 1i*randn(N,N,F);
D2 = randn(N,N,F) + 1i*randn(N,N,F);

disp(version)

% test pagemrdivide
testpagefunc("pagemrdivide",D1,D2);

% test pagemldivide
testpagefunc("pagemldivide",D1,D2);

% test pagemldivide
testpagefunc("pagemtimes",D1,D2);

function testpagefunc(func,D1,D2)
normalfunc = strrep(func,"page","");
Do = feval(func,D1,D2);
Do_t1 = zeros(size(Do));
Do_t2 = zeros(size(Do));
for ff = 1 : size(D1,3)
    Do_t1(:,:,ff) = feval(func,D1(:,:,ff),D2(:,:,ff));
    Do_t2(:,:,ff) = feval(normalfunc,D1(:,:,ff),D2(:,:,ff));
end
err_ml_1 = max(abs((Do(:) - Do_t1(:))));
err_ml_2 = max(abs((Do(:) - Do_t2(:))));
disp("Difference between "+func+" and "+func+" in for-loop: "+err_ml_1)
disp("Difference between "+func+" and "+normalfunc+" in for-loop: "+err_ml_2)

end


