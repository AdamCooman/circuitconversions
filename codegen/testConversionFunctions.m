% This function takes the conversion functions and transforms some random
% data a bunch of times. We check whether the result after these
% transformations is still close to the original data
clear variables
close all
clc

addpath("..")
% N is the number of ports
N = 2;
F = 300;
WeHaveAnError = false;
Representations = ["S" "Y" "Z" "T" "G" "H" "A" "B"];
for ii = 1 : 1000
    % generate some random data
    [data,freq] = GenerateRandomData(F,"Random");
    % copy the data on which we will work
    data_after = data;
    % start with a random representation
    startrep  = find(Representations=="Y");
    path = string(Representations(startrep));
    currentrep  = startrep;
    % go to a few other representations with different reference impedances
    for pp = 1 : 4
        nextrep = currentrep;
        while nextrep == currentrep || nextrep == startrep
            nextrep = randi(numel(Representations),1);
        end
        path = path+"-"+string(Representations(nextrep));
        data_after = feval("Convert."+Representations(currentrep)+"to"+Representations(nextrep),data_after);
        currentrep  = nextrep;
    end
    % go back to the original representation and reference impedance
    data_after = feval("Convert."+Representations(currentrep)+"to"+Representations(startrep),data_after);
    path = path+"-"+string(Representations(startrep));
    % check the error
    err = max(abs(data_after(:)-data(:)));
    if isnan(err)
        fprintf( 2,"%s\n","error in path: "+path+" NaN value in chain");
        WeHaveAnError = true;
    else
        if err > 1e-6
            fprintf( 2,"%s\n","error in path: "+path+" max error: "+err);
            WeHaveAnError = true;
        else
            fprintf( 1,"%s\n"," correct path: "+path+" max error: "+err);
        end
    end
end

if WeHaveAnError
    fprintf( 2,"%s\n","Tests finished with an error");
else
    fprintf( 1,"%s\n","All Tests finished successfully");
end

function [Y,freq] = GenerateRandomData(F,Source)
arguments
    F (1,1) double
    Source (1,1) string {mustBeMember(Source,["Circuit" "Random"])}
end
switch Source
    case "Random"
        Y = randn(2,2,F) + 1i*randn(2,2,F);
        freq = reshape(linspace(0,1,F),1,1,F);
    case "Circuit"
        % generates some random data from a random RLC pi network
        R1 = 100*rand()+0.001;
        R2 = 100*rand()+0.001;
        Rt = 100*rand()+0.001;
        C1 = (100*rand()+0.001)*1e-9;
        C2 = (100*rand()+0.001)*1e-9;
        Ct = (100*rand()+0.001)*1e-9;
        L1 = (100*rand()+0.001)*1e-9;
        L2 = (100*rand()+0.001)*1e-9;
        Lt = (100*rand()+0.001)*1e-9;
        freq = reshape(logspace(4,9,F),1,1,[]);
        s = (1i*2*pi).*freq;
        Yr = [R1+Rt, -Rt;
            -Rt  , R2+Rt];
        Yc = [C1+Ct, -Ct;
            -Ct  , C2+Ct];
        Yl = [L1+Lt, -Lt;
            -Lt  , L2+Lt];
        Y = Yr + s.*Yc + 1./(s.*Yl);
end
end

function PlotDataMagni(freq,data,col)
subplot(121)
semilogx(squeeze(freq),20*log10(abs(squeeze(data(1,:,:)).')),col)
hold on
semilogx(squeeze(freq),20*log10(abs(squeeze(data(2,:,:)).')),col)
grid on
end
function PlotDataPhase(freq,data,col)
subplot(122)
semilogx(squeeze(freq),angle(squeeze(data(1,:,:)).'),col)
hold on
semilogx(squeeze(freq),angle(squeeze(data(2,:,:)).'),col)
grid on
end