% In this script, we auto-generate the conversion functions using matlab's symbolic toolbox.
% We also verify whether the conversion matrices are correct
clear variables
close all
clc

% define the voltage and current variables
symbols.V1 = sym("V_1");
symbols.I1 = sym("I_1");
symbols.V2 = sym("V_2");
symbols.I2 = sym("I_2");
% define the characteristic impedances
symbols.Z1 = sym("Z_1");
symbols.Z2 = sym("Z_2");
symbols.k1 = 1/2/sqrt(real(symbols.Z1));
symbols.k2 = 1/2/sqrt(real(symbols.Z2));
% construct the waves
symbols.A1 = symbols.k1*(symbols.V1+symbols.Z1*symbols.I1);
symbols.B1 = symbols.k1*(symbols.V1-symbols.Z1*symbols.I1);
symbols.A2 = symbols.k2*(symbols.V2+symbols.Z2*symbols.I2);
symbols.B2 = symbols.k2*(symbols.V2-symbols.Z2*symbols.I2);

% define all the inputs and outputs of the different representations
Representations.Y.in  = [symbols.V1;symbols.V2];
Representations.Y.out = [symbols.I1;symbols.I2];

Representations.Z.in  = [symbols.I1;symbols.I2];
Representations.Z.out = [symbols.V1;symbols.V2];

Representations.S.in  = [symbols.A1;symbols.A2];
Representations.S.out = [symbols.B1;symbols.B2];

Representations.H.in  = [symbols.I1;symbols.V2];
Representations.H.out = [symbols.V1;symbols.I2];

Representations.G.in  = [symbols.V1;symbols.I2];
Representations.G.out = [symbols.I1;symbols.V2];

Representations.A.in  = [symbols.V2;-symbols.I2];
Representations.A.out = [symbols.V1;symbols.I1];

Representations.B.in  = [symbols.V1;symbols.I1];
Representations.B.out = [symbols.V2;-symbols.I2];

Representations.T.in  = [symbols.B2;symbols.A2];
Representations.T.out = [symbols.A1;symbols.B1];

% Z and Y, G and H, A and B are inverses of eachother
KnownMatrices.GtoH = sym([zeros(2) eye(2);eye(2) zeros(2)]);
KnownMatrices.AtoB = sym([zeros(2) eye(2);eye(2) zeros(2)]);
KnownMatrices.YtoZ = sym([zeros(2) eye(2);eye(2) zeros(2)]);
% Define Y to S
KnownMatrices.YtoS = [
    -symbols.k1*symbols.Z1 0      symbols.k1 0;
    0      -symbols.k2*symbols.Z2 0  symbols.k2;
    symbols.k1*symbols.Z1 0       symbols.k1 0;
    0      symbols.k2*symbols.Z2  0  symbols.k2;
    ];
% Define Y to G
KnownMatrices.YtoG = sym([1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0]);
% Y to H is now the cascade of Y to G and G to H
KnownMatrices.YtoH = KnownMatrices.GtoH * KnownMatrices.YtoG;
% Define Y to A
KnownMatrices.YtoA = sym([0 0 1 0;1 0 0 0;0 0 0 1;0 -1 0 0]);
% Y to B is now the cascade of Y to A and A to B
KnownMatrices.YtoB = KnownMatrices.AtoB * KnownMatrices.YtoA;
% S to T can be done
KnownMatrices.StoT = sym([0 0 1 0;1 0 0 0;0 1 0 0;0 0 0 1]);
KnownMatrices.YtoT = KnownMatrices.StoT * KnownMatrices.YtoS;
% add all the inverse transforms
KnownMatrices = AddInverses(KnownMatrices);


% we now know how to go from any representation to Y
% and because we know the inverse, we can get to any representation from Y
% we can therefore now compute all the transforms
KnownMatrices = CompleteMatrices(KnownMatrices,Representations);
% sort the fields accoding to name
KnownMatrices = orderfields(KnownMatrices);
% we check whether all matrices are correct
checkAllMatrices(KnownMatrices,Representations)
% generate the conversion functions
GenerateFunctions(KnownMatrices,[sym("Z_1") sym("Z_2")]);

%% Functions to generate the conversion matrices 
function KnownMatrices = CompleteMatrices(KnownMatrices,Representations)
% Adds all the transformations that are possible
for From = string(fieldnames(Representations)).'
    for To = string(fieldnames(Representations)).'
        if To ~= From
            if ~isfield(KnownMatrices,From+"to"+To)
                KnownMatrices.(From+"to"+To) = KnownMatrices.("Yto"+To) * KnownMatrices.(From+"toY");
            end
        end
    end
end
end
function KnownMatrices = AddInverses(KnownMatrices)
% Adds the inverse transformation when it is not in the struct
for field = string(fieldnames(KnownMatrices)).'
    T = regexp(field,"(?<In>.+)to(?<Out>.+)","names");
    if ~isfield(KnownMatrices,T.Out+"to"+T.In)
        KnownMatrices.(T.Out+"to"+T.In) = inv(KnownMatrices.(field));
    end
end
end
function checkAllMatrices(KnownMatrices,Representations)
% Function to loop over all conversion matrices and check whether it is correct
fields = fieldnames(KnownMatrices);
for field = string(fields).'
    T = regexp(field,"(?<In>.+)to(?<Out>.+)","names");
    checkMatrix(KnownMatrices,Representations,T.In,T.Out)
end
end
function checkMatrix(KnownMatrices,Representations,In,Out)
% Function to check whether the matrix of a conversion is correct
U = cat(1,Representations.(In).out,Representations.(In).in);
Ut_test = KnownMatrices.(In+"to"+Out)*U;
Ut_corr = cat(1,Representations.(Out).out,Representations.(Out).in);
res = Ut_test - Ut_corr;
res = simplify(res);
if any(res~=0)
    warning("There seems to be an error in "+In+" to "+Out);
    disp("We were expecting: ")
    pretty(simplify(Ut_corr))
    disp("But we got: ")
    pretty(simplify(Ut_test))
end
end
%% Code generation functions 
function GenerateFunctions(KnownMatrices,Zref)
% Generate the code to perform the transformation
dirname = "+generated_conversion";
if ~isfolder(dirname)
    mkdir(dirname);
end
for field = string(fieldnames(KnownMatrices)).'
    filename = fullfile(dirname,field+".m");
    matlabFunction(KnownMatrices.(field),"File",filename,"Vars",{Zref});
    [from,to]=GetRepresentationsFromField(field);
    % re-read the function and add the wrapper around it
    code = readlines(filename);
    % throw away the lines with comments
    code = code(~startsWith(code,"%"));
    code = code(code~="");
    % replace the function name
    code(1) = strrep(code(1),field,"getP_"+field);
    code(end+1) = "end";%#ok
    % append the top function
    code = [%#ok
        "function [D,varargout] = "+field+"(D,Zref,dD)"
        "% "+field+" converts from "+from+"-parameters to "+to+"-parameters"
        "%"
        "%      "+to+" = "+field+"("+from+")"
        "%      "+to+" = "+field+"(D"+from+",Zref)"
        "%      ["+to+",d"+to+"] = "+field+"("+from+",Zref,d"+from+")"
        "%"
        "% where"
        "%  "+from+" is a (2x2xF) matrix with the "+from+"-parameters over frequency"
        "%  Zref is a scalar or (1x2) vector with the reference impedance per port"
        "%  d"+from+" is a (2x2xF) matrix with the derivative of the "+from+"-parameters over frequency"
        "%"
        "% Adam Cooman"
        "% This code is auto-generated by the generateConversion script of the CircuitTools toolbox"
        "arguments"
        "    D (2,2,:) double"
        "    Zref (1,:) double = 50"
        "end"
        "arguments (Repeating)"
        "    dD (2,2,:) double"
        "end"
        "    if isscalar(Zref)"
        "        Zref = repmat(Zref,1,2);";
        "    else"
        "        assert(numel(Zref)==2);";
        "    end"
        "P = getP_"+field+"(Zref);";
        "P11 = P(1:2,1:2);"
        "P12 = P(1:2,3:4);"
        "P21 = P(3:4,1:2);"
        "P22 = P(3:4,3:4);"
        "if isempty(dD)"
        "    % D' = (P11*D+P12)/(P21*D+P22)"
        "    D = pagemrdivide(pagemtimes(P11,D) + P12, pagemtimes(P21,D)+P22);"
        "else"
        "    % B = (P21*D+P22)^-1"
        "    B = pageinv(pagemtimes(P21,D)+P22);"
        "    % D' = (P11*D+P12)*B";
        "    D = pagemtimes( pagemtimes(P11,D)+P12 ,B);"
        "    % A = (P11-D'*P21)"
        "    A = P11-pagemtimes(D,P21);"
        "    % dD' = A * dD * B"
        "    varargout = cellfun(@(dR) pagemtimes(A,pagemtimes(dR,B)),dD,'UniformOutput',false);"
        "end";
        "end";
        code    
    ];
    writelines(code,filename);
end
end
function [from,to]=GetRepresentationsFromField(field)
field = strsplit(field,"to");
from = field(1);
to = field(2);
end